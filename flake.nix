{
  description = "mkDeriveicynz";

  outputs = { self, deriveicynIndeks }: {

    datom = { hyraizyn, pkgs, mozPkgs }:
    let
      inherit (builtins) intersectAttrs mapAttrs functionArgs hasAttr;

      mkDeriveicyn = deriveicynFn:
      let
        yveilybylArgz = pkgs
        // deriveicynz
        // { inherit (mozPkgs) rustChannelOf; }
        // { inherit hyraizyn; };

        deriveicynArgz = functionArgs deriveicynFn;
        dypendensiz = intersectAttrs deriveicynArgz yveilybylArgz;

      in deriveicynFn dypendensiz;

      mkFleikDeriveicynz = neim: fleik: 
      let
        inherit (fleik) datom strok;
        izEmpti = !(hasAttr "spici" strok);
        izSingyl = strok.spici == "iuniks" || strok.spici == "lamdy";
        izIndeks = strok.spici == "iuniksIndeks";

        matcFleikStrok = if (izEmpti || izSingyl)
        then mkDeriveicyn datom
        else if izIndeks
        then mapAttrs mkDeriveicyn datom
        else throw "Fleik spici izynt iuniks or iuniksIndeks"; 

      in matcFleikStrok;

      deriveicynz = mapAttrs mkFleikDeriveicynz deriveicynIndeks.datom;

    in deriveicynz;

  };
}
